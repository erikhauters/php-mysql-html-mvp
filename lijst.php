<?php

/* Mysql parameters */
$servername = "localhost";
$username = "root";
$password = "root";
$dbname = "php-mysql-html-mvp";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

/* Select all info from db */
$sql = "SELECT * FROM leerkrachten ORDER BY leerkracht";
// $sql = "SELECT leerkracht, klas, uur FROM leerkrachten ORDER BY leerkracht";
// $sql = "SELECT l.*, v.leerkracht as vervanger FROM leerkrachten AS l LEFT JOIN leerkrachten AS v ON v.id=l.vervanging ORDER BY l.leerkracht";
$result = $conn->query($sql);
$conn->close();
?>
<html>
  <head>
    <title>Afwezigheden</title>
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">    
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
 
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <h3>Leerkrachten</h3>
    <div class="content">
      <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
        <thead>
          <tr>
            <th>Leerkracht</th>
            <th>Uur</th>
            <th>Klas</th>
            <th>Vervanging</th>
          </tr>
        </thead>
        <tbody>
        <?php 
          /* Loop over results */
          while($row = $result->fetch_assoc()) { ?>        
          <tr>
            <td><?= $row['leerkracht']; ?></td>
            <td><?= $row['uur']; ?></td>
            <td><?= $row['klas']; ?></td>
            <td><?= $row['vervanging']; ?></td>
          </tr>
        <?php 
          /* END LOOP */
          } 
        ?>
          
        </tbody>
      </table>
      <button id="add-leerkracht" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored">
        <i class="material-icons">add</i>
      </button>
    </div>
  </body>
</html>
